let mobilenet;
let classifier;
let video;
let label = 'test';
let ukeButton;
let whistleButton;
let trainButton;

function modelReady() {
  console.log('Model is ready!!!');
}

function videoReady() {
  console.log('Video is ready!!!');
}

function whileTraining(loss) {
  if (loss == null) {
    console.log('Training Complete');
    classifier.classify(gotResults);
  } else {
    console.log(loss);
  }
}


function gotResults(error, result) {
  if (error) {
    console.error(error);
  } else {
    label = result;
    classifier.classify(gotResults);
  }
}

function setup() {
  let canvas = createCanvas(500, 500);
  canvas.parent('container');
  video = createCapture(VIDEO);
  video.hide();
  background(0);
  mobilenet = ml5.featureExtractor('MobileNet', modelReady);
  classifier = mobilenet.classification(video, videoReady);

  ukeButton = createButton('gaurav');
  ukeButton.mousePressed(function() {
    classifier.addImage('gaurav');
  });
  ukeButton.parent('button-container');
  whistleButton = createButton('manasmadhar');
  whistleButton.mousePressed(function() {
    classifier.addImage('manas madharchod');
  });
  whistleButton.parent('button-container');
  trainButton = createButton('train');
  trainButton.mousePressed(function() {
    classifier.train(whileTraining);
  });
  trainButton.parent('train-button-container');
  classifier.predict(image, function(err, results) {
    result.innerText = results[0].className;
    probability.innerText = results[0].probability.toFixed(4);
  });


}

function draw() {
  background(0);
  image(video, 0, 0, 500, 480);
  fill(255);
  textSize(16);
  text(label, 10, height - 10);
}

